var mode = true;
var maps = 1;

$(document).ready(function () {
    $(".item").on("mouseover", function () {
        //$(this).find(".dimness").css("background-color", "rgba(0,0,0,0.7)");
    });
    $(".item").on("mouseout", function () {
        //$(this).find(".dimness").css("background-color", "rgba(0,0,0,0.5)");
    });

    $(".item").on("click", function () {
        if ($(this).find(".choice").length === 0) {
            if (mode) {
                var img = $('<img class="banned choice">');
                img.attr('src', 'images/banned.png');
                $(this).prepend(img);
                $(this).find(".dimness").css("background-color", "rgba(200,0,0,0.5)");
                $(this).find(".name").hide();
            } else {
                $(this).find(".dimness").css("background-color", "rgba(0,200,0,0.5");
                var span = $('<p class="picked choice">');
                span.html("Mapa "+maps);      
                $(this).find(".dimness").append(span);
                maps++;
            }
        } else {
            console.log(mode);
            $(this).find(".choice").remove();
            $(this).find(".name").show();
            $(this).find(".dimness").css("background-color", "rgba(0,0,0,0.5)");
            if(!mode){
                maps--;
            }
        }
    });

    $(".switch").on("click", function () {
        if (mode) {
            $(this).find("img").attr("src", "images/on.png");
            mode = false;
        } else {
            $(this).find("img").attr("src", "images/off.png");
            mode = true;
        }
    });

});